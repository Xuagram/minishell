/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/21 13:42:30 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/25 14:13:41 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <unistd.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include "libft.h"
# include "get_next_line.h"
# include "ft_printf.h"
# define NO_FILE 	1
# define GETCWD_ERR 2
# define MALLOC_ERR 3
# define ALNUM_ERR 4
# define BEGIN_ERR 5
# define UNDEF_VAR 6
# define UNSETENV_ERR 7
# define SETENV_MANY 8
# define USER_ERR 9
# define MANY_ARG 10
# define NOT_DIR 11
# define NO_PERM 12
# define SYNTAX_ERR 13
# define NO_CMD 14
# define INV_OPT 15
# define FORK_ERR 17
# define WAIT_ERR 18
# define INVAL_ARG 19
# define LSTAT_ERR 21
# define CD_INVAL 22
# define OLDPWD_ERR 24
# define SYNT_DOLLAR 25
# define DQUOTE_ERR 26
# define SQUOTE_ERR 27
# define TRUE 1
# define FALSE 0

typedef	struct	s_option
{
	char		*opt;
	char		opterr;
	int			optind;
}				t_option;

typedef	struct	s_err
{
	int			no;
	char		msge[28][1024];
	char		*var;
}				t_err;

typedef struct	s_cmd
{
	char		**env_cp;
	char		**cmd;
	char		*bin_path;
	t_err		*err;
	int			lnk;
	t_option	*opt;
}				t_cmd;

/*
** parsing.c
*/
int				get_size(char **tab, int i);

/*
** init.c
*/
t_cmd			*set_arg(t_cmd *arg, char **env, char **argv);

/*
** minishell.c
*/
int				minishell(t_cmd *arg);

/*
** parsing.c
*/
char			**get_cmd(char **env, t_err *err);
char			**get_env(char **env, int i);

/*
**	expansions.c
*/
void			expansions(char **str, char **env_cp, t_err *err);
char			*concat_params(char **tab, t_err *err, int space, int n);

/*
** check_dollar.c
*/
int				check_dollar(char *str);

/*
** echo.c
*/
int				echo(t_cmd *arg);

/*
** chg_dir.c
*/
int				chg_dir(t_cmd *arg);
void			chg_dir_mv(t_cmd *arg, char *dir, char **env_cp, t_err *err);

/*
** set_pwd.c
*/
void			set_pwd(char **env_cp, char path[4096], int *errnum);
void			set_oldpwd(char **env_cp, int *errnum);
void			get_lnk_path(char *dir, char current[4096], char path[4096]);
void			is_lnk(t_cmd *arg, struct stat data, char *dir, t_err *err);
void			cd_hyphen(t_cmd *arg, char **dir, t_err *err);

/*
** unsetenv.c
*/
int				unset_env(t_cmd *arg);
char			*find_var(char **env_cp, char *ref, t_err *err);

/*
** valid_var.c
*/
int				check_valid_var(char *ref, t_err *err);
void			cd_no_arg(t_cmd *arg);

/*
** env.c
*/
int				env(t_cmd *arg);

/*
** get_opt.c
*/
int				ft_getopt(int ac, char **av, const char *opt_ref,
		t_option *opt);

/*
** built_in.c
*/
int				check_ref(t_cmd *arg);
int				built_in(t_cmd *arg, int i);

/*
** set_env.c
*/
int				set_env(t_cmd *arg);
char			**add_env(char **env, char *var, char *val, t_err *err);
char			**check_env(char **env, char *var, char *val, t_err *err);

/*
** quote.c
*/
char			*manage_quote(char *str, t_err *err, char *final);

/*
** exit.c
*/
int				builtin_exit(t_cmd *arg);

/*
** path.c
*/
int				find_path(char **env, char *cmd, char **bin_path, t_err *err);

/*
** concat_path.c
*/
void			concat_path(char *cmd, char *path, char **bin_path, t_err *err);
int				is_err(t_err *err);

/*
** binary.c
*/
void			fork_prompt(char **argv, char **environ, char **bin_path,
		t_err *err);

/*
** error.c
*/
void			print_error(t_err *err);
void			error_cmd(t_cmd *arg);

#endif
