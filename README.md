# Minishell

This project written in C is a minimum viable version of a real shell. 

This is the second project at 42 school in Unix system specialization. 

Any system call cannot be used to realize this project. We are restricted to use a certain list of functions in order to give us a better understanding of what is happening 'in the back'. You can find more details on the limits set by the subject in this repository in the file : `/subject/minishell.en.pdf`

I use my own version of the library functions, that I have in the `libft/` directory which includes a 'homemade' `printf`.
 
# Features

Implement a series of builtins: 
```
echo 
cd 
setenv
unsetenv
env
exit
```


# How to run

This program shell can be compiled on all unix system.

- Clone this repository : `git clone https://gitlab.com/Xuagram/minishell.git`

- Change directory into the directory and run make
```
cd minishell
make
```

- Run 

`./minishell`


Enjoy !
