/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   built_in.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 12:18:47 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:32:51 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	set_tab(int (*builtins[6])(t_cmd *arg))
{
	builtins[0] = chg_dir;
	builtins[1] = unset_env;
	builtins[2] = env;
	builtins[3] = set_env;
	builtins[4] = echo;
	builtins[5] = builtin_exit;
}

static void	set_ref(char builtins_ref[7][9])
{
	ft_strcpy(builtins_ref[0], "cd");
	ft_strcpy(builtins_ref[1], "unsetenv");
	ft_strcpy(builtins_ref[2], "env");
	ft_strcpy(builtins_ref[3], "setenv");
	ft_strcpy(builtins_ref[4], "echo");
	ft_strcpy(builtins_ref[5], "exit");
	ft_bzero(builtins_ref[6], 9);
}

int			check_ref(t_cmd *arg)
{
	char	builtins_ref[7][9];
	int		i;

	set_ref(builtins_ref);
	i = 0;
	while (builtins_ref[i][0] && ft_strcmp(builtins_ref[i], arg->cmd[0]))
		i++;
	if (!builtins_ref[i][0])
		i = -1;
	return (i);
}

int			built_in(t_cmd *arg, int i)
{
	int		(*builtins[6])(t_cmd *arg);

	set_tab(builtins);
	if (builtins[i](arg))
		return (-1);
	return (0);
}
