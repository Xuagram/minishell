/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 15:43:28 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 17:11:54 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*find_var(char **env_cp, char *ref, t_err *err)
{
	char	*ret;
	int		end;
	int		i;

	i = 0;
	ret = NULL;
	end = (ft_strichr_alnum(ref) < ft_strichr(env_cp[i], '=')) ?
	ft_strichr(env_cp[i], '=') : ft_strichr_alnum(ref);
	while (env_cp[i] && ft_strncmp(env_cp[i], ref, end))
	{
		i++;
		end = (ft_strichr_alnum(ref) < ft_strichr(env_cp[i], '=')) ?
		ft_strichr(env_cp[i], '=') : ft_strichr_alnum(ref);
	}
	if (env_cp[i])
	{
		ret = ft_strchr(env_cp[i], '=');
		if (*ret)
			ret++;
	}
	if (ret && ref[ft_strichr_alnum(ref)] == '$')
		err->no = SYNT_DOLLAR;
	return (ret);
}

int		unset_env(t_cmd *arg)
{
	int		i;
	int		j;
	char	**tmp;
	int		size;

	i = -1;
	j = -1;
	tmp = arg->env_cp;
	if (arg->cmd[1])
	{
		size = get_size(arg->env_cp, 0);
		if (!(arg->env_cp = ft_memalloc((size + 1) * sizeof(char *))))
			((t_err *)arg->err)->no = MALLOC_ERR;
		while (tmp[++i])
			if (ft_strncmp(arg->cmd[1], tmp[i], ft_strlen(arg->cmd[1])))
				if (!(arg->env_cp[++j] = ft_strdup(tmp[i])))
					return (-3);
		arg->env_cp[++j] = 0;
		ft_freetab((void **)tmp);
	}
	else
		((t_err *)arg->err)->no = UNSETENV_ERR;
	return (0);
}
