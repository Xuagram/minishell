/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/21 15:01:13 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/27 12:12:25 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static	char	**replace_exp(char **cmd, char **env, t_err *err)
{
	int i;

	i = 0;
	while (cmd[i])
	{
		expansions(&cmd[i], env, err);
		i++;
	}
	return (cmd);
}

int				get_size(char **tab, int i)
{
	unsigned int range;

	range = 0;
	if (tab)
	{
		while (tab[i])
		{
			i++;
			range++;
		}
	}
	return (range);
}

char			**get_env(char **env, int i)
{
	char			**env_cp;
	unsigned int	size;
	int				j;

	env_cp = NULL;
	j = 0;
	size = get_size(env, i);
	if (!(env_cp = ft_memalloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (j < (int)size)
	{
		if (!(env_cp[j] = ft_strdup(env[i])))
			return (NULL);
		j++;
		i++;
	}
	env_cp[j] = NULL;
	return (env_cp);
}

char			**get_cmd(char **env, t_err *err)
{
	int		fd;
	char	*line;
	char	**tab;

	line = NULL;
	tab = NULL;
	fd = 1;
	get_next_line(fd, &line);
	if (!line)
		exit(0);
	if (!(tab = ft_strsplit(ft_space(line), ' ')))
		err->no = MALLOC_ERR;
	ft_memdel((void **)&line);
	if (!err->no)
		tab = replace_exp(tab, env, err);
	return (tab);
}
