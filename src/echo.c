/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 10:56:58 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:52:05 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	join_tab(int space, char **str, char *tab_line, t_err *err)
{
	char *tmp;
	char *add_space;

	tmp = NULL;
	add_space = NULL;
	if (!(tmp = ft_strdup(*str)))
		err->no = MALLOC_ERR;
	if (*str)
		ft_memdel((void **)str);
	if (space)
	{
		if (!(add_space = ft_strjoin(tab_line, " \0")))
			err->no = MALLOC_ERR;
		if (!(*str = ft_strjoin_free(&tmp, add_space)))
			err->no = MALLOC_ERR;
		ft_memdel((void **)&add_space);
	}
	else
	{
		if (!(*str = ft_strjoin_free(&tmp, tab_line)))
			err->no = MALLOC_ERR;
	}
}

static char	*make_str(char **tab, t_err *err, int space, int n)
{
	char	*str;
	int		i;
	int		count;

	count = 0;
	i = n;
	str = NULL;
	while (tab[i])
		count = ft_strlen(tab[i++]) + count + space;
	if (!(str = ft_strnew(count + 1)))
		err->no = MALLOC_ERR;
	i = n;
	while (!err->no && tab[i])
	{
		if (i == get_size(tab, 0) - 1)
			space = 0;
		join_tab(space, &str, tab[i++], err);
	}
	return (str);
}

/*
** concat params take variable n to set its index to
** go through the array tab. First it mallocs a string
** of the size of the addition of every elements of the
** array. Then it calls the function join_tab that
** concatenes the content of the array to one and single string.
*/

char		*concat_params(char **tab, t_err *err, int space, int n)
{
	char	*str;

	str = NULL;
	if (get_size(tab, n) == 1)
	{
		if (!(str = ft_strdup(tab[n])))
			err->no = MALLOC_ERR;
	}
	else
		str = make_str(tab, err, space, n);
	return (str);
}

int			echo(t_cmd *arg)
{
	char	*str;
	int		n;

	str = NULL;
	n = 1;
	if (arg->cmd[1])
	{
		if (!ft_strcmp(arg->cmd[1], "-n"))
			n = 2;
		str = concat_params(arg->cmd, arg->err, 1, n);
		if (str && n == 2)
			ft_putstr(str);
		else if (str)
			ft_putendl(str);
		if (str)
			ft_memdel((void **)&str);
	}
	return (((t_err *)arg->err)->no);
}
