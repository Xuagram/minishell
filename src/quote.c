/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quote.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 16:59:32 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/25 14:55:20 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*match_on(char *match, char *str)
{
	if (*str == '\'')
	{
		*match = '\'';
		str++;
	}
	else if (*str == '"')
	{
		*match = '"';
		str++;
	}
	return (str);
}

static char	*match_off(char *match, char *str)
{
	if (*match == *str)
	{
		*match = ' ';
		str++;
	}
	return (str);
}

static void	get_quote_err(t_err *err, char match)
{
	if (match == '"')
		err->no = DQUOTE_ERR;
	if (match == '\'')
		err->no = SQUOTE_ERR;
}

char		*manage_quote(char *str, t_err *err, char *final)
{
	int		j;
	char	match;

	j = 0;
	match = ' ';
	while (str && *str)
	{
		if (match == ' ')
		{
			str = match_on(&match, str);
			if (*str && match == ' ')
				final[j++] = *str++;
		}
		if (match == '\'' || match == '"')
		{
			str = match_off(&match, str);
			if (*str && match != ' ')
				final[j++] = *str++;
		}
	}
	get_quote_err(err, match);
	final[j] = '\0';
	return (final);
}
