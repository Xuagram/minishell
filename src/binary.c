/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/21 14:58:46 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/27 12:45:46 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** 1. The function fork_prompt will be called upon the case where the command
** tries to execute a binary. Fork will duplicate the calling process between
** a child and a parent process. The PID of the child processus is returned.
** 2. Wait will suspend the execution of the calling thread until one of its
** children terminates.
** 3. The system call execve executes the binary executable '*bin_path' given
** along with its array of arguments strings 'argv' passed to the new program.
** By convention, the first of these strings should contain the filename
** associated with the file being executed. 'environ' is an array of strings,
** passed as environment to the new program.
*/

void	fork_prompt(char **argv, char **environ, char **bin_path, t_err *err)
{
	pid_t	init;
	int		nb;

	nb = 1;
	if ((init = fork()) < 0)
		err->no = FORK_ERR;
	if (!err->no && init > 0)
	{
		if (wait(&nb) < 0)
			err->no = WAIT_ERR;
	}
	if (!err->no && init == 0)
		if (execve(*bin_path, argv, environ) < 0)
			err->no = NO_CMD;
	ft_memdel((void **)bin_path);
}
