/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 16:06:43 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:53:39 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	exec_prog(char **env, t_cmd *arg, t_err *err, int i)
{
	char **tmp;
	char *bin_path;

	bin_path = NULL;
	tmp = get_env(arg->cmd, i);
	if (find_path(arg->env_cp, arg->cmd[i], &bin_path, err))
		fork_prompt(tmp, env, &bin_path, err);
	else
	{
		err->no = NO_FILE;
		if (!(err->var = ft_strdup(arg->cmd[i])))
			err->no = MALLOC_ERR;
	}
	ft_freetab((void **)tmp);
	ft_freetab((void **)env);
}

void		invalid_arg(char *arg, t_err *err)
{
	err->no = INVAL_ARG;
	if (!(err->var = ft_strdup(arg)))
		err->no = MALLOC_ERR;
}

char		**split_val_var(char **env, char *var, t_err *err)
{
	char *tmp;

	tmp = NULL;
	if (!(tmp = ft_strsub(var, 0, ft_strichr(var, '='))))
		err->no = MALLOC_ERR;
	if (!err->no)
	{
		env = check_env(env, tmp, ft_strchr(var, '=') + 1, err);
		ft_strdel(&tmp);
	}
	return (env);
}

void		cp_env(t_cmd *arg, t_err *err, t_option *opt)
{
	char **env_tmp;

	env_tmp = NULL;
	if (*opt->opt)
		env_tmp = NULL;
	else
		env_tmp = get_env(arg->env_cp, 0);
	while (!err->no && arg->cmd[opt->optind]
		&& ft_strichr(arg->cmd[opt->optind], '=') > 0)
	{
		env_tmp = split_val_var(env_tmp, arg->cmd[opt->optind], err);
		opt->optind++;
	}
	if (!err->no && arg->cmd[opt->optind]
		&& ft_strichr(arg->cmd[opt->optind], '=') == 0)
		invalid_arg(arg->cmd[opt->optind], err);
	if (!err->no && !arg->cmd[opt->optind] && env_tmp)
		ft_print_words_tables(env_tmp);
	else if (!err->no && arg->cmd[opt->optind])
		exec_prog(env_tmp, arg, err, opt->optind);
}

int			env(t_cmd *arg)
{
	t_option	*opt;
	t_err		*err;

	opt = arg->opt;
	err = arg->err;
	if (get_size(arg->cmd, 0) == 1)
		ft_print_words_tables(arg->env_cp);
	else
	{
		if ((err->no = ft_getopt(get_size(arg->cmd, 0), arg->cmd,
		"i", arg->opt)))
		{
			if (!(err->var = ft_strdup(&opt->opterr)))
				err->no = MALLOC_ERR;
		}
		else
			cp_env(arg, arg->err, arg->opt);
	}
	return (err->no);
}
