/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_env.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/25 12:40:06 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/25 14:55:53 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** copy_env adds an '=' sign between the variable and the value.
** If there is no value it just adds an equal to the variable name.
*/

static char	*copy_env(char *var, char *val, t_err *err)
{
	char *dest;
	char *dest2;

	dest = NULL;
	dest2 = NULL;
	if (val)
	{
		if (!(dest = ft_strjoin(var, "=")))
			err->no = MALLOC_ERR;
		if (!(dest2 = ft_strjoin(dest, val)))
			err->no = MALLOC_ERR;
		ft_strdel(&dest);
	}
	else if (!(dest2 = ft_strjoin(var, "=")))
		err->no = MALLOC_ERR;
	return (dest2);
}

char		**add_env(char **env, char *var, char *val, t_err *err)
{
	int		i;
	int		size;
	char	**tmp;

	i = -1;
	tmp = get_env(env, 0);
	size = get_size(env, 0);
	ft_freetab((void **)env);
	if (!(env = (char **)ft_memalloc((size + 2) * sizeof(char *))))
		err->no = MALLOC_ERR;
	while (tmp[++i])
	{
		if (!(env[i] = ft_strdup(tmp[i])))
			err->no = MALLOC_ERR;
	}
	if (!(env[i] = copy_env(var, val, err)))
		err->no = MALLOC_ERR;
	env[i + 1] = 0;
	ft_freetab((void **)tmp);
	return (env);
}

/*
** check_env checks if the variable that is set with setenv
** already exist or not. If it exists it will call copy_env.
** Otherwise it will call add_env to 'realloc' the array of
** environment variables.
*/

char		**check_env(char **env, char *var, char *val, t_err *err)
{
	int		i;
	char	*final;

	i = 0;
	final = NULL;
	if (val)
		if (!(final = ft_strdup(val)))
			err->no = MALLOC_ERR;
	if (!err->no && !(check_valid_var(var, err)))
	{
		if (val && (ft_strichr(val, '"') >= 0 || ft_strichr(val, '\'' >= 0)))
			final = manage_quote(val, err, final);
		while (!err->no && env && env[i]
				&& ft_strncmp(var, env[i], ft_strichr(env[i], '=')))
			i++;
		if (!err->no && env && env[i])
		{
			ft_memdel((void **)&env[i]);
			env[i] = copy_env(var, final, err);
		}
		else if (!err->no)
			env = add_env(env, var, final, err);
		ft_strdel(&final);
	}
	return (env);
}

/*
** If there is no argument with the setenv builtin, it just
** prints the array of environment variables. If there is more than
** two arguments with the command setenv, it will display an error message.
** Otherwise it will calls the function check_env.
*/

int			set_env(t_cmd *arg)
{
	if (!arg->cmd[1])
		ft_print_words_tables(arg->env_cp);
	else if (get_size(arg->cmd, 1) <= 2)
		arg->env_cp = check_env(arg->env_cp, arg->cmd[1],
				arg->cmd[2], arg->err);
	else
		((t_err *)arg->err)->no = SETENV_MANY;
	return (((t_err *)arg->err)->no);
}
