/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 19:16:30 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/25 14:02:10 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	set_err_msg_cd(char msge[28][1024])
{
	ft_strcpy(msge[22], "cd: -");
	ft_strcpy(msge[23], ": invalid option\ncd: usage: cd [-L|-P] [dir]");
	ft_strcpy(msge[24], "cd: OLDPWD not set");
	ft_strcpy(msge[25], "Syntax Error.");
	ft_strcpy(msge[26], "Unmatched \".");
	ft_strcpy(msge[27], "Unmatched \'.");
}

void	set_err_msge(char msge[28][1024])
{
	ft_strcpy(msge[0], "none");
	ft_strcpy(msge[1], ": No such file or directory.");
	ft_strcpy(msge[2], "getcwd: failed");
	ft_strcpy(msge[3], "malloc: failed");
	ft_strcpy(msge[4],
		"setenv: Variable name must contain alphanumeric characters.");
	ft_strcpy(msge[5], "setenv: Variable name must begin with a letter.");
	ft_strcpy(msge[6], ": Undefined variable.");
	ft_strcpy(msge[7], "unsetenv: Too few arguments.");
	ft_strcpy(msge[8], "setenv: Too many arguments.");
	ft_strcpy(msge[9], "Unknown user: ");
	ft_strcpy(msge[10], "cd: Too many arguments.");
	ft_strcpy(msge[11], ": Not a directory.");
	ft_strcpy(msge[12], ": Permission denied.");
	ft_strcpy(msge[13], "exit: Expression Syntax.");
	ft_strcpy(msge[14], ": Command not found.");
	ft_strcpy(msge[15], "env: illegal option -- ");
	ft_strcpy(msge[16],
		"\nusage: env [-i] [name=value ...] [utility [argument ...]]");
	ft_strcpy(msge[17], "fork: failed");
	ft_strcpy(msge[18], "wait: failed");
	ft_strcpy(msge[19], "env: setenv ");
	ft_strcpy(msge[20], ": Invalid argument");
	ft_strcpy(msge[21], "lstat: failed");
	set_err_msg_cd(msge);
}

t_cmd	*set_arg(t_cmd *arg, char **env, char **argv)
{
	(void)argv;
	arg = ft_memalloc(sizeof(t_cmd));
	arg->env_cp = NULL;
	arg->cmd = NULL;
	arg->err = ft_memalloc(sizeof(t_err));
	arg->opt = ft_memalloc(sizeof(t_option));
	((t_err *)arg->err)->var = NULL;
	((t_option *)arg->opt)->opt = NULL;
	set_err_msge(((t_err *)arg->err)->msge);
	if (!(arg->env_cp = get_env(env, 0)))
		((t_err *)arg->err)->no = MALLOC_ERR;
	return (arg);
}
