/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_var.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 12:13:43 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 17:13:08 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	cd_no_arg(t_cmd *arg)
{
	char *dir;

	dir = NULL;
	if ((dir = find_var(arg->env_cp, "HOME", arg->err)))
		chg_dir_mv(arg, dir, arg->env_cp, arg->err);
	else
	{
		((t_err *)arg->err)->no = UNDEF_VAR;
		if (!(((t_err *)arg->err)->var = ft_strdup("HOME")))
			((t_err *)arg->err)->no = MALLOC_ERR;
	}
}

/*
** They are three different cases that invalid a variable :
** UNDEF_VAR, BEGIN_ERR and ALNUM_ERR.
** In all cases, it will be invalid because of the presence of
** a non alphanumeric character. If the name of the variable
** calls a variable with $ it goes to var_exist.
** If there is a non alphanumeric character at the beggining
** of the string it will display the second case of error.
** In all other cases it will be the ALNUM_ERR message of error.
*/

int		check_valid_var(char *ref, t_err *err)
{
	char *tmp;

	tmp = NULL;
	if (!(tmp = ft_strdup(ref)))
		err->no = MALLOC_ERR;
	if (!err->no && !ft_isalnum(ref[0]))
		err->no = BEGIN_ERR;
	while (!err->no && *ref && ft_isalnum(*ref))
		ref++;
	if (!err->no && *ref)
		if (!ft_isalnum(*ref))
			err->no = ALNUM_ERR;
	if (tmp)
		free(tmp);
	return (err->no);
}
