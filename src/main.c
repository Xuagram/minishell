/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/21 13:35:11 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 15:51:38 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	main(int argc, char **argv, char **env)
{
	t_cmd *arg;

	arg = NULL;
	if (!(arg = set_arg(arg, env, argv)))
		print_error(arg->err);
	while (argc)
	{
		if (minishell(arg))
			print_error(arg->err);
	}
	return (0);
}
