/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 14:56:54 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/27 13:06:24 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** build_path tries to concatenate every path in $PATH var with
** the command given. If a command is found, it will check if
** the path of the directory has the execution rights, which is
** necessary to execute the file.
*/

static void	build_path(char *cmd, char **path, char **bin_path, t_err *err)
{
	int i;

	i = 0;
	if (!path || !path[0])
		return ;
	concat_path(cmd, path[i], bin_path, err);
	while (path[1] && access(*bin_path, F_OK) && path[++i])
		concat_path(cmd, path[i], bin_path, err);
	if (path && (!path[i] || access(*bin_path, F_OK)))
		ft_memdel((void **)bin_path);
	else if (access(path[i], X_OK))
	{
		if (!(err->var = ft_strdup(*bin_path)))
			err->no = MALLOC_ERR;
		err->no = NO_PERM;
	}
}

/*
** In the first place perm_path isolate the path of the directory
** where the binary is. It checks if the directory has the execution
** permission. Otherwise it checks if the file has the execution
** permission.
*/

static void	perm_exec(char *bin_path, t_err *err)
{
	if (!err->no && access(bin_path, X_OK))
	{
		if (!(err->var = ft_strdup(bin_path)))
			err->no = MALLOC_ERR;
		err->no = NO_PERM;
	}
}

static int	perm_path(char *cmd, t_err *err)
{
	int		len;
	char	*tmp;

	len = ft_strlen(cmd);
	tmp = NULL;
	while (len && cmd[len] != '/')
		len--;
	if (len)
	{
		if (!(tmp = ft_strsub(cmd, 0, len)))
			err->no = MALLOC_ERR;
		else if (access(tmp, X_OK))
		{
			if (!(err->var = ft_strdup(cmd)))
				err->no = MALLOC_ERR;
			err->no = NO_PERM;
		}
		free(tmp);
	}
	return (err->no);
}

static int	no_path(char *cmd, char **bin_path, t_err *err)
{
	if (!*bin_path)
	{
		if (!(*bin_path = ft_strjoin("./", cmd)))
			err->no = MALLOC_ERR;
		if (access(*bin_path, F_OK))
			ft_memdel((void **)bin_path);
	}
	if (!is_err(err) && *bin_path)
	{
		perm_exec(*bin_path, err);
		if (err->no)
			ft_memdel((void **)bin_path);
		else
			return (1);
	}
	return (0);
}

/*
** If the argument given is a path, it will check if the path has the
** permission and if the binary exists.
** If the argument given is a binary name without path, it wil sets
** an array from the different different path contained in the variable
** PATH.
** If the PATH variable is unset or there is no binary found in the PATH
** array, it will try to check the existence of the binary in the current
** directory with no_path.
** the function no_path will set the return of the function find_path with
** 1 if a valid binary has been found and the relevant path will be attributed
** to the bin_path variable. Otherwise, the function will return 0.
*/

int			find_path(char **env, char *cmd, char **bin_path, t_err *err)
{
	int		ret;
	char	**path;

	ret = 0;
	path = NULL;
	if (!is_err(err) && ft_strichr(cmd, '/') >= 0 && !perm_path(cmd, err)
			&& !access(cmd, F_OK))
	{
		if (!(*bin_path = ft_strdup(cmd)))
			err->no = MALLOC_ERR;
	}
	else if (!err->no)
	{
		if (!err->no && find_var(env, "PATH", err)
			&& !(path = ft_strsplit(find_var(env, "PATH", err), ':')))
			err->no = MALLOC_ERR;
		if (!err->no && path)
			build_path(cmd, path, bin_path, err);
	}
	ret = no_path(cmd, bin_path, err);
	ft_freetab((void**)path);
	return (ret);
}
