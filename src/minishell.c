/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 16:59:57 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/25 19:02:24 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	minishell(t_cmd *arg)
{
	int index;

	index = 0;
	ft_printf("$> ");
	arg->cmd = get_cmd(arg->env_cp, arg->err);
	if (arg->cmd && !((t_err *)arg->err)->no && arg->cmd[0])
	{
		if (!((t_err *)arg->err)->no && (index = check_ref(arg)) >= 0)
			built_in(arg, index);
		else if (!((t_err *)arg->err)->no && find_path(arg->env_cp,
			arg->cmd[0], &arg->bin_path, arg->err))
			fork_prompt(arg->cmd, arg->env_cp, &arg->bin_path, arg->err);
		else if (!((t_err *)arg->err)->no)
			error_cmd(arg);
	}
	arg->cmd = (char **)ft_freetab((void **)arg->cmd);
	ft_memdel((void **)&((t_option *)arg->opt)->opt);
	return (((t_err *)arg->err)->no);
}
