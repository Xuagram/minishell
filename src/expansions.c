/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/26 11:04:52 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 15:47:12 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*replace_tilde(char **str, char *var, t_err *err)
{
	char *end;

	end = NULL;
	if (!(end = ft_strsub(*str, 1, ft_strlen(*str))))
		err->no = MALLOC_ERR;
	ft_memdel((void **)str);
	if (!err->no)
	{
		if (!(*str = ft_strjoin(var, end)))
			err->no = MALLOC_ERR;
		ft_memdel((void **)&end);
	}
	return (*str);
}

static void	tilde(char **str, char **env_cp, t_err *err)
{
	char *var;

	var = NULL;
	if (ft_strlen(*str) == 1 || *(*str + 1) == '/')
	{
		if (!(var = find_var(env_cp, "HOME", err)))
		{
			err->no = UNDEF_VAR;
			if (!(err->var = ft_strdup("HOME")))
				err->no = MALLOC_ERR;
		}
		replace_tilde(str, var, err);
	}
	else
	{
		if (!(err->var = ft_strdup(*str)))
			err->no = MALLOC_ERR;
		err->no = USER_ERR;
	}
}

static char	*replace_var(char **str, char *var, t_err *err)
{
	char *end;

	end = NULL;
	if (!(end = ft_strsub(*str, ft_strichr_alnum(*str), ft_strlen(*str))))
		err->no = MALLOC_ERR;
	ft_strdel(str);
	if (!err->no)
	{
		if (!(*str = ft_strjoin(var, end)))
			err->no = MALLOC_ERR;
		ft_strdel(&end);
	}
	return (*str);
}

static void	dollar(char **str, char **env_cp, t_err *err)
{
	char	**tab;
	char	*var;
	int		i;

	tab = NULL;
	var = NULL;
	if (!(tab = ft_strsplit_end(*str, '$')))
		err->no = MALLOC_ERR;
	i = get_size(tab, 0) - ft_count_char(*str, '$') - 1;
	i = (str[0][ft_strlen(*str) - 1] == '$') ? i + 1 : i;
	ft_memdel((void **)str);
	while (!err->no && tab[++i])
	{
		if (!(var = find_var(env_cp, tab[i], err)) && !err->no)
		{
			err->no = UNDEF_VAR;
			if (!(err->var = ft_strdup(tab[i])))
				err->no = MALLOC_ERR;
		}
		else if (!err->no)
			tab[i] = replace_var(&tab[i], var, err);
	}
	*str = concat_params(tab, err, 0, 0);
	ft_freetab((void **)tab);
}

void		expansions(char **str, char **env_cp, t_err *err)
{
	if (!err->no && ft_strichr(*str, '$') >= 0)
	{
		if (ft_strlen(*str) > 1)
		{
			if (!check_dollar(*str))
				dollar(str, env_cp, err);
			else
				err->no = SYNT_DOLLAR;
		}
	}
	if (!err->no && *str && **str == '~')
		tilde(str, env_cp, err);
}
