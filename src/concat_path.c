/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   concat_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 12:45:02 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:44:44 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** is err checks if the error flag of NO_CMD that checks
** if the PATH environment variable exists is on or not.
** The binary should be able to be executed if the path
** of the binary is already given in the cmd although there
** is no PATH environment variable.
*/

int		is_err(t_err *err)
{
	if (!err->no || err->no == NO_CMD)
		return (0);
	return (1);
}

void	concat_path(char *cmd, char *path, char **bin_path, t_err *err)
{
	char *tmp;

	tmp = NULL;
	ft_memdel((void **)bin_path);
	if (!(tmp = ft_strjoin(path, "/")))
		err->no = MALLOC_ERR;
	if (!(*bin_path = ft_strjoin_free(&tmp, cmd)))
		err->no = MALLOC_ERR;
}
