/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 13:15:55 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:54:27 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	error_cmd(t_cmd *arg)
{
	((t_err *)arg->err)->no = NO_CMD;
	if (!(((t_err *)arg->err)->var = ft_strdup(arg->cmd[0])))
		((t_err *)arg->err)->no = MALLOC_ERR;
}

void	invopt(t_err *err)
{
	ft_putstr_fd(err->msge[err->no], 2);
	ft_putstr_fd(err->var, 2);
	ft_putendl_fd(err->msge[err->no + 1], 2);
	ft_memdel((void **)&err->var);
}

void	print_error(t_err *err)
{
	if (err->no == NO_FILE || err->no == NOT_DIR || err->no == NO_PERM
		|| err->no == NO_CMD || err->no == UNDEF_VAR)
	{
		ft_putstr_fd(err->var, 2);
		ft_putendl_fd(err->msge[err->no], 2);
		ft_memdel((void **)&err->var);
	}
	else if (err->no == USER_ERR)
	{
		ft_putstr_fd(err->msge[err->no], 2);
		ft_putstr_fd(err->var + 1, 2);
		ft_putendl_fd(".", 2);
		ft_memdel((void **)&err->var);
	}
	else if (err->no == INV_OPT || err->no == INVAL_ARG || err->no == CD_INVAL)
		invopt(err);
	else
		ft_putendl_fd(err->msge[err->no], 2);
	err->no = 0;
}
