/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chg_dir.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 12:40:05 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:44:03 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	get_path(t_cmd *arg, char *dir, char current[4096], t_err *err)
{
	char path[4096];
	char pwd[4103];

	ft_bzero(pwd, 4103);
	ft_bzero(path, 4096);
	if (arg->lnk && ft_strcmp(((t_option *)arg->opt)->opt, "P")
			&& ft_strcmp(((t_option *)arg->opt)->opt, "LP"))
		get_lnk_path(dir, current, path);
	if (find_var(arg->env_cp, "PWD", err))
		set_pwd(arg->env_cp, path, &err->no);
	else if (!err->no)
	{
		if (!path[0])
			if (!getcwd(path, 4096))
				err->no = GETCWD_ERR;
		ft_strcpy(pwd, "PWD");
		arg->env_cp = add_env(arg->env_cp, pwd, path, err);
	}
}

/*
** chg_dir sets the old path variable to the current path.
** It changes the directory. It sets the environment
** variable PWD to the current PWD. It sets the number
** of error to the appropriate error.
*/

void		chg_dir_mv(t_cmd *arg, char *dir, char **env_cp, t_err *err)
{
	char current[4096];

	ft_bzero(current, 4096);
	if (!getcwd(current, 4096))
		err->no = GETCWD_ERR;
	if (find_var(env_cp, "OLDPWD", err))
		set_oldpwd(env_cp, &err->no);
	if (dir && chdir(dir) < 0)
	{
		err->no = NO_FILE;
		if (!(err->var = ft_strdup(dir)))
			err->no = MALLOC_ERR;
	}
	if (!err->no)
		get_path(arg, dir, current, err);
}

static void	chg_dir_perm(t_cmd *arg, char *dir, t_err *err)
{
	struct stat data;

	if (!err->no && stat(dir, &data))
	{
		if (!(err->var = ft_strdup(dir)))
			err->no = MALLOC_ERR;
		err->no = NO_PERM;
	}
	if (!err->no && !S_ISDIR(data.st_mode))
	{
		if (!(err->var = ft_strdup(dir)))
			err->no = MALLOC_ERR;
		err->no = NOT_DIR;
	}
	if (!err->no && access(dir, X_OK))
	{
		if (!(err->var = ft_strdup(dir)))
			err->no = MALLOC_ERR;
		err->no = NO_PERM;
	}
	if (!err->no)
		is_lnk(arg, data, dir, err);
	if (!err->no)
		chg_dir_mv(arg, dir, arg->env_cp, err);
}

/*
** chg_dir_exist manages the case of the 'cd -' to
** redirect to $OLDPWD. If the variable doesn't exist
** there is an error message.
** The function then checks if the directory does exist,
** through access function.
*/

static void	chg_dir_exist(t_cmd *arg, t_err *err)
{
	t_option	*opt;

	opt = arg->opt;
	if (!ft_strcmp(arg->cmd[opt->optind], "-"))
		cd_hyphen(arg, &arg->cmd[opt->optind], err);
	if (!err->no && access(arg->cmd[opt->optind], F_OK))
	{
		if (!(err->var = ft_strdup(arg->cmd[opt->optind])))
			err->no = MALLOC_ERR;
		err->no = NO_FILE;
	}
	else if (!err->no)
		chg_dir_perm(arg, arg->cmd[opt->optind], arg->err);
}

int			chg_dir(t_cmd *arg)
{
	t_option	*opt;
	t_err		*err;

	opt = arg->opt;
	err = arg->err;
	if ((err->no = ft_getopt(get_size(arg->cmd, 0), arg->cmd,
		"LP", arg->opt)) == CD_INVAL)
	{
		if (!(err->var = ft_strdup(&opt->opterr)))
			err->no = MALLOC_ERR;
	}
	else if (get_size(arg->cmd, opt->optind) > 2)
		err->no = MANY_ARG;
	else if (arg->cmd[opt->optind])
		chg_dir_exist(arg, arg->err);
	else
		cd_no_arg(arg);
	return (err->no);
}
