/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_pwd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 12:24:16 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 17:08:48 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	cd_hyphen(t_cmd *arg, char **dir, t_err *err)
{
	char *tmp;

	tmp = NULL;
	tmp = find_var(arg->env_cp, "OLDPWD", err);
	if (!err->no && !tmp)
		err->no = OLDPWD_ERR;
	else if (!err->no)
	{
		ft_memdel((void **)dir);
		if (!(*dir = ft_strdup(tmp)))
			err->no = MALLOC_ERR;
		if (!err->no)
			ft_putendl(*dir);
	}
}

void	is_lnk(t_cmd *arg, struct stat data, char *dir, t_err *err)
{
	if (!lstat(dir, &data))
	{
		if (S_ISLNK(data.st_mode))
			arg->lnk = 1;
	}
	else
		err->no = LSTAT_ERR;
}

void	set_pwd(char **env_cp, char path[4096], int *errnum)
{
	int i;

	i = 0;
	while (env_cp[i] && ft_strncmp(env_cp[i], "PWD=", 4))
		i++;
	if (env_cp[i])
	{
		if (!path[0])
		{
			if (!getcwd(path, 4096))
				*errnum = GETCWD_ERR;
		}
		ft_memdel((void **)&env_cp[i]);
		if (!(env_cp[i] = ft_strjoin("PWD=", path)))
			*errnum = MALLOC_ERR;
	}
}

void	set_oldpwd(char **env_cp, int *errnum)
{
	int		i;
	char	tmp[4105];
	char	path[4096];

	i = 0;
	ft_bzero(tmp, 4105);
	ft_bzero(path, 4096);
	if (!getcwd(path, 4096))
		*errnum = GETCWD_ERR;
	if (env_cp[i])
		ft_strcpy(tmp, ft_strcat(ft_strcpy(tmp, "OLDPWD="), path));
	i = 0;
	while (env_cp[i] && ft_strncmp(env_cp[i], "OLDPWD=", 7))
		i++;
	if (env_cp[i])
	{
		ft_memdel((void **)&env_cp[i]);
		if (!(env_cp[i] = ft_strdup(tmp)))
			*errnum = MALLOC_ERR;
	}
}

void	get_lnk_path(char *dir, char current[4096], char path[4096])
{
	int j;
	int len;

	j = 0;
	len = ft_strlen(current);
	while (dir[j] && (!ft_strncmp(dir + j, "../", 3)
				|| !ft_strncmp(dir + j, "./", 2)))
	{
		if (!ft_strncmp(dir + j, "./", 2))
			j = j + 2;
		else
		{
			while (len && current[len] != '/')
				len--;
			if (current[len] == '/')
				len--;
			j = j + 3;
		}
	}
	ft_strncpy(path, current, len + 1);
	ft_strcat(ft_strcat(path, "/"), dir + j);
}
