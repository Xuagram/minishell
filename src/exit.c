/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 11:20:58 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:57:06 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	free_var(t_cmd *arg)
{
	t_option *opt;

	opt = arg->opt;
	arg->env_cp = (char **)ft_freetab((void **)arg->env_cp);
	arg->cmd = (char **)ft_freetab((void **)arg->cmd);
	if (opt->opt)
		ft_memdel((void **)&opt->opt);
}

int			builtin_exit(t_cmd *arg)
{
	int i;

	i = 0;
	if (get_size(arg->cmd, 0) > 2)
		((t_err *)arg->err)->no = SYNTAX_ERR;
	else if (arg->cmd[1])
	{
		while (arg->cmd[1][i] && ft_isdigit(arg->cmd[1][i]))
			i++;
		if (arg->cmd[1][i])
			((t_err *)arg->err)->no = SYNTAX_ERR;
	}
	if (!((t_err *)arg->err)->no && (!arg->cmd[1] || !arg->cmd[1][i]))
	{
		i = 0;
		if (arg->cmd[1])
			i = ft_atoi(arg->cmd[1]);
		free_var(arg);
		ft_putendl("exit");
		exit(i);
	}
	return (((t_err *)arg->err)->no);
}
