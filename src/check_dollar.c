/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_dollar.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:37:20 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:37:35 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	check_dollar(char *str)
{
	int i;
	int err;
	int len;

	err = 0;
	i = 0;
	len = ft_strlen(str);
	while (i + 1 < len && !err)
	{
		if (str[i] == '$' && str[i + 1] == '$')
			err = 1;
		i++;
	}
	return (err);
}
