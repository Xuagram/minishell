/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   apply_flag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 12:06:57 by temehenn          #+#    #+#             */
/*   Updated: 2019/08/20 11:20:32 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*apply_prec(char *var, char conv, long int prec, int neg)
{
	char	*new;

	new = 0;
	if (((prec == 0 && (var[0] == '0' || conv == 's'))
		|| (conv == 's' && prec != 0))
			&& conv != 'f' && prec > 0)
		ft_strclr(var + prec);
	else if (conv == 'i' || conv == 'd' || conv == 'o' || conv == 'u'
			|| conv == 'x' || conv == 'X')
	{
		if ((long int)ft_strlen(var) <= prec)
			return (integer_prec(new, prec, neg, var));
	}
	ft_memmove(var, var + neg, ft_strlen(var));
	return (var);
}

int		calc_len(char *var, t_arg arg, t_first_c *first_c)
{
	first_c->len = 0;
	first_c->len = (arg.sharp && (arg.conv == 'x'
				|| arg.conv == 'X' || arg.conv == 'p')) ? 2 : first_c->len;
	first_c->len = ((arg.sharp && arg.conv == 'o')
			|| arg.space || arg.plus || first_c->neg) ? 1 : first_c->len;
	if ((long int)ft_strlen(var) < arg.field)
		first_c->space = arg.field - ft_strlen(var) - first_c->len;
	else
		first_c->space = 0;
	if (first_c->space < 0)
		first_c->space = 0;
	return (first_c->space);
}

char	*padding(char *new, int zero, long int space, unsigned long int len)
{
	unsigned long int i;

	i = len;
	while (i < space + len)
	{
		if (zero)
			new[i] = '0';
		else
			new[i] = ' ';
		i++;
	}
	return (new);
}

char	*left_padding(char *var, t_arg arg, t_first_c first_c, char *new)
{
	if (first_c.space && !(arg.less) && arg.zero)
	{
		put_first_char(arg, first_c, new);
		new = padding(new, arg.zero, first_c.space, first_c.len);
		ft_strcpy(new + first_c.space + first_c.len, var);
		return (new);
	}
	else if (first_c.space && !(arg.less) && !(arg.zero))
	{
		new = padding(new, arg.zero, first_c.space, 0);
		put_first_char(arg, first_c, new + first_c.space);
		ft_strcpy(new + first_c.space + first_c.len, var);
		return (new);
	}
	return (var);
}

char	*put_string(char *var, t_arg arg, t_first_c first_c)
{
	char	*new;

	new = 0;
	if (arg.field == 0)
		arg.field = ft_strlen(var);
	if (!(new = ft_strnew(arg.field + 1)))
		return (NULL);
	if (first_c.space && !(arg.less))
	{
		new = left_padding(var, arg, first_c, new);
		ft_strdel(&var);
		return (new);
	}
	put_first_char(arg, first_c, new);
	ft_strncpy(new + first_c.len, var, ft_strlen(var));
	new[first_c.len + ft_strlen(var)] = 0;
	new = padding(new, arg.zero, first_c.space, ft_strlen(new));
	ft_strdel(&var);
	return (new);
}
