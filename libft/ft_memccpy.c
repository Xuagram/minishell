/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 15:38:15 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 17:57:09 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char		*tmp_dest;
	const unsigned char *tmp_src;
	unsigned char		a;

	tmp_dest = dest;
	tmp_src = src;
	a = (unsigned char)c;
	while (n)
	{
		*tmp_dest = *tmp_src;
		if (*tmp_src == a)
			return (tmp_dest + 1);
		tmp_dest++;
		tmp_src++;
		n--;
	}
	return (NULL);
}

/*
**int	main(int argc, char **argv)
**{
**	argc = 0;
**	char str [20] = "In da club";
**	char str2 [20] = "In da club";
**	puts(ft_memccpy(str, argv[1], 'd', 4));
**	puts(memccpy(str2, argv[1], 'd', 4));
**	return (0);
**}
*/
