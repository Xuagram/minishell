/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_pd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 13:55:02 by temehenn          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*boring_case(char *str, int i, char flag)
{
	int	j;

	(void)i;
	j = (flag == '-') ? 2 : 1;
	if (flag == '-')
	{
		ft_memmove(str + 2, str + 1, ft_strlen(str));
		str[1] = '1';
	}
	else
	{
		ft_memmove(str + 1, str, ft_strlen(str));
		str[0] = '1';
	}
	while (str[j])
	{
		if (str[j] != '.')
			str[j] = '0';
		j++;
	}
	return (str);
}

static char	*apply_arrond(char *str, int i)
{
	if (i == 0 && str[i] == '9')
		boring_case(str, i, '+');
	else if (i == 1 && str[i] == '9' && str[i - 1] == '-')
		boring_case(str, i, '-');
	else if (str[i] < '9' && str[i] != '.')
		str[i]++;
	else if (str[i] == '9')
	{
		str[i] = '0';
		return (apply_arrond(str, i - 1));
	}
	else if (str[i] == '.')
		return (apply_arrond(str, i - 1));
	return (str);
}

char		*fill_deci(char *res, long double var, long int prec)
{
	int				i;
	int				pos;
	int				ret;
	unsigned int	tmp;

	i = 0;
	ret = 0;
	pos = ft_strlen(res);
	var *= 10;
	while (i++ < prec)
	{
		tmp = var;
		if (tmp >= 10)
			tmp = 9;
		res[pos] = tmp + '0';
		ret = 10 * tmp;
		var = 10 * var - ret;
		pos++;
	}
	if (arrond(var))
	{
		res = apply_arrond(res, ft_strlen(res) - 1);
	}
	return (res);
}

char		*fill_pd(char *res, long double var, long int prec, int sharp)
{
	int				i;
	long long int	yolo;

	i = 0;
	if (var < 0)
		var = -var;
	yolo = var;
	if (var > 0)
		var -= yolo;
	if (var == 0.0 && prec == 0)
	{
		while (res[i] && res[i] != '.')
			i++;
		if (res[i] == '.' && !sharp)
			res[i] = 0;
		return (res);
	}
	res = fill_deci(res, var, prec);
	return (res);
}
