/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 21:01:13 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/23 16:56:49 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strchr_end(const char *s, int c)
{
	unsigned char		letter;
	char				*tmp;

	letter = c;
	tmp = (char *)s;
	while (*tmp && (*tmp != letter || (*tmp == letter && !*(tmp + 1))))
		tmp++;
	if (*tmp != letter)
		return (NULL);
	return ((char *)tmp);
}
/*
**int	main(int argc, char **argv)
**{
**	ft_printf("%s", ft_strchr(argv[1], ft_atoi(argv[2])));
**	ft_printf("%s", strchr(argv[1], ft_atoi(argv[2])));
**}
*/
