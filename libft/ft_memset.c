/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 15:26:35 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/25 14:06:59 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char tmp_c;
	unsigned char *str;

	tmp_c = c;
	str = b;
	while (len--)
	{
		*str = tmp_c;
		str++;
	}
	return (b);
}

/*
**int	main(int argc, char **argv)
**{
**	char s1[20] = "tsubarashi";
**	ft_putstr(ft_memset(s1, ft_atoi(argv[1]), ft_atoi(argv[2])));
**	return (0);
**}
*/
