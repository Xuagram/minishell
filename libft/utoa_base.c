/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/22 15:46:44 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		fill_number(char *res, unsigned long long int var,
		unsigned int base, int len)
{
	char	*s;

	s = "0123456789abcdef";
	if (var >= base)
		fill_number(res, var / base, base, len - 1);
	res[len] = s[var % base];
}

static int		get_len(unsigned long long int var, int base)
{
	int count;

	count = 0;
	while (var)
	{
		count++;
		var = var / base;
	}
	return (!count) ? 1 : count;
}

char			*utoa_base(unsigned long long int var, int base)
{
	char			*res;
	int				len;

	len = get_len(var, base);
	if (!(res = ft_strnew(len + 1)))
		return (0);
	fill_number(res, var, base, len - 1);
	return (res);
}
