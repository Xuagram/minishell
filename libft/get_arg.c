/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_arg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/22 14:59:35 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*get_signed_conv(t_arg *arg, va_list ap)
{
	long long int	var;
	char			*s;

	var = 0;
	if (!ft_strcmp(arg->len, "h"))
		var = (short int)va_arg(ap, int);
	else if (!ft_strcmp(arg->len, "hh"))
		var = (signed char)va_arg(ap, int);
	else if (!ft_strcmp(arg->len, "l"))
		var = (long int)va_arg(ap, long int);
	else if (!ft_strcmp(arg->len, "ll"))
		var = (long long int)va_arg(ap, long long int);
	else
		var = (int)va_arg(ap, int);
	if (!(s = itoa_base(var, 10)))
		exit(0);
	return (s);
}

char	*get_unsigned_conv(t_arg *arg, va_list ap)
{
	unsigned long long int	var;
	char					*s;

	if (!ft_strcmp(arg->len, "h"))
		var = (unsigned short int)va_arg(ap, unsigned int);
	else if (!ft_strcmp(arg->len, "hh"))
		var = (unsigned char)va_arg(ap, unsigned int);
	else if (!ft_strcmp(arg->len, "l") || arg->conv == 'p')
		var = (unsigned long int)va_arg(ap, unsigned long int);
	else if (!ft_strcmp(arg->len, "ll"))
		var = (unsigned long long int)va_arg(ap, unsigned long long int);
	else
		var = (unsigned int)va_arg(ap, unsigned int);
	if (arg->conv == 'o')
		if (!(s = utoa_base(var, 8)))
			exit(0);
	if (arg->conv == 'x' || arg->conv == 'X' || arg->conv == 'p')
		if (!(s = utoa_base(var, 16)))
			exit(0);
	if (arg->conv == 'u')
		if (!(s = utoa_base(var, 10)))
			exit(0);
	if ((arg->conv == 'x' || arg->conv == 'X') && var == 0)
		arg->sharp = 0;
	return (s);
}

char	*get_digit_conv(t_arg *arg, va_list ap)
{
	char	*s;

	s = 0;
	if (arg->conv == 'd' || arg->conv == 'i')
		s = get_signed_conv(arg, ap);
	else
		s = get_unsigned_conv(arg, ap);
	return (s);
}

char	*check_long(va_list ap, t_arg *arg)
{
	long double	a;
	char		*s;

	s = 0;
	arg->prec = (arg->prec != -1) ? arg->prec : 6;
	if (arg->conv == 'f' && arg->len[0] != 'L')
	{
		a = va_arg(ap, double);
		if (div_case(a, &s))
			return (s);
		s = float_conv(a, arg);
	}
	else
	{
		a = va_arg(ap, long double);
		if (div_case(a, &s))
			return (s);
		s = float_conv(a, arg);
	}
	return (s);
}

char	*get_arg(va_list ap, t_arg *arg)
{
	char	*s;

	s = 0;
	if (arg->conv == 'c')
		s = c_conv(s, ap, arg);
	else if (arg->conv == 's')
	{
		if (!(s = va_arg(ap, char *)))
		{
			if (!(s = ft_strdup("(null)")))
				exit(0);
		}
		else if (!(s = ft_strdup(s)))
			exit(0);
	}
	else if (is_unsigned_conv(arg->conv) || arg->conv == 'i'
			|| arg->conv == 'd' || arg->conv == 'p')
		s = get_digit_conv(arg, ap);
	else
		s = check_long(ap, arg);
	if (arg->null == 1)
		arg->field--;
	return (s);
}
