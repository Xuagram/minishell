/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 20:22:53 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/16 16:53:50 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

static int	find_end_line(char **ptr, char **line, int ret)
{
	char	*ptr2;

	ptr2 = NULL;
	if (ft_strlen(*ptr) == 0)
		return (0);
	if ((ptr2 = ft_strchr(*ptr, '\n')))
	{
		*ptr2 = '\0';
		*line = *ptr;
		if ((*ptr = ft_strdup(ptr2 + 1)) == 0)
			return (-1);
		return (1);
	}
	else if (!ret && *ptr)
	{
		*line = *ptr;
		*ptr = NULL;
		return (1);
	}
	else if (*ptr && ret)
		return (2);
	return (0);
}

void		clean_str(void *content, size_t size)
{
	free(((t_fd*)content)->str);
	(void)size;
}

int			multi_fd_lst(t_list **current, t_list **head, const int fd)
{
	t_fd	file;

	file.nb = fd;
	file.str = NULL;
	*current = *head;
	if (current)
		while (*current && (((t_fd*)(*current)->content)->nb != fd))
			*current = (*current)->next;
	if (!(*current))
	{
		if (!(*current = ft_lstnew(&file, sizeof(file))))
		{
			ft_lstdel(head, clean_str);
			return (-1);
		}
		ft_lstadd(head, *current);
		return (1);
	}
	if (((t_fd*)(*current)->content)->str)
		return (1);
	return (0);
}

int			read_only(int fd, t_list **current, char **line)
{
	char				buff[BUFF_SIZE + 1];
	int					ret;
	int					err;
	t_fd				*ptr;
	char				*tmp;

	ptr = (t_fd*)(*current)->content;
	while ((ret = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[ret] = '\0';
		if (!(tmp = ft_strjoin(ptr->str, buff)))
			return (-1);
		free(ptr->str);
		ptr->str = tmp;
		if ((find_end_line(&(ptr->str), line, ret)) == 1)
			return (1);
	}
	if (ret == -1)
		return (-1);
	else if (ptr->str != NULL
			&& ((err = find_end_line(&(ptr->str), line, ret)) == -1))
		return (-1);
	else if (ptr->str != NULL && err)
		return (1);
	return (err);
}

int			get_next_line(const int fd, char **line)
{
	static t_list		*head = NULL;
	t_list				*current;
	int					ret;

	if (!(ret = multi_fd_lst(&current, &head, fd)))
		return (0);
	else if (ret == -1 || fd < 0)
		return (-1);
	if ((ret = read_only(fd, &current, line)) == -1)
	{
		ft_lstdel(&head, clean_str);
		return (-1);
	}
	return (ret);
}

/*
**#include <fcntl.h>
**#include <stdio.h>
**int		main(int argc, char **argv)
**{
**	char	*line = NULL;
**	argc = 0;
**	int fd = open(argv[1], O_RDONLY);
**	int fd2 = open(argv[2], O_RDONLY);
**	int fd3 = open(argv[3], O_RDONLY);
**	int fd4 = open(argv[4], O_RDONLY);
**	if	(get_next_line(fd, &line) > 0)
**	{
**		ft_printf("fd1 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd, &line) > 0)
**	{
**		ft_printf("fd1 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd, &line) > 0)
**	{
**		ft_printf("fd1 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd2, &line) > 0)
**	{
**		ft_printf("fd2 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd3, &line) > 0)
**	{
**		ft_printf("fd3 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd, &line) > 0)
**	{
**		ft_printf("fd1 result = %s\n", line);
**		free(line);
**	}
**	if	(get_next_line(fd4, &line) > 0)
**	{
**		ft_printf("fd4 result = %s\n", line);
**		free(line);
**	}
**	close(fd);
**	close(fd2);
**	close(fd3);
**	close(fd4);
**	return(0);
**}
*/
