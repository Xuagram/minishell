/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 12:48:11 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 14:06:17 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	if (s1 && s2)
	{
		if (!n)
			return (1);
		while (*s1 == *s2 && *s1 && *s2 && --n)
		{
			s1++;
			s2++;
		}
		if ((*s1 == '\0' && *s2 == '\0') || *s1 == *s2)
			return (1);
		else
			return (0);
	}
	return (0);
}

/*
**static  int	main(int argc, char **argv)
**{
** ft_strenqu(argv[1], argv[2]);
** return (0);
**}
*/
