/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 15:29:00 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/22 15:32:01 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int i;
	int len;

	i = 0;
	if (!s)
		return (NULL);
	len = ft_strlen(s) - 1;
	while (len >= 0 && (s[len] == ' ' || s[len] == '\n' || s[len] == '\t'))
		len--;
	while (i < len && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
		i++;
	if (len < 0)
		return (ft_strdup(""));
	return (ft_strsub(s, i, len + 1 - i));
}

/*
**static int	main(int argc, char **argv)
**{
**	ft_putstr(ft_strtrim("  \t \t \n   \n\n\n\t"));
**	ft_putstr("\n");
**	ft_putstr(ft_strtrim(""));
**	return(0);
**}
*/
