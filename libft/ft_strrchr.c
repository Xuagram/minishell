/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 21:01:13 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strrchr(const char *s, int c)
{
	const char	*tmp;
	char		letter;

	tmp = NULL;
	letter = c;
	while (*s)
	{
		if (*s == letter)
			tmp = s;
		s++;
	}
	if (letter == '\0')
		tmp = s;
	return ((char *)tmp);
}

/*
**#include <stdio.h>
**#include <string.h>
**
**int	main(void)
**{
**	char *str = "there is so \0ma\0ny \0 \\0 in t\0his stri\0ng !\0\0\0\0";
**	ft_printf("%p\n", ft_strrchr(str, '\0'));
**	ft_printf("%p\n", strrchr(str, '\0'));
**}
*/
