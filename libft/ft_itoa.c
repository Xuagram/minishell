/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 11:14:33 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 18:43:38 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static size_t	ft_count_size(int n, int *neg)
{
	long int	nb;
	size_t		size;

	size = 1;
	nb = n;
	if (nb < 0)
	{
		nb *= -1;
		size++;
		*neg = 1;
	}
	while (nb >= 10)
	{
		size++;
		nb = nb / 10;
	}
	return (size);
}

char			*ft_itoa(int n)
{
	char		*new;
	size_t		size;
	long int	nb;
	int			neg;

	neg = 0;
	size = ft_count_size(n, &neg);
	nb = n;
	if (nb < 0)
		nb = -nb;
	if (!(new = (ft_strnew(size))))
		return (NULL);
	while (size > 0)
	{
		new[--size] = nb % 10 + 48;
		nb = nb / 10;
	}
	if (neg)
		new[size] = '-';
	return (new);
}

/*
**int	main(int argc, char **argv)
**{
**	argc = 0;
**	ft_putstr(ft_itoa(ft_atoi(argv[1])));
**	return (0);
**}
*/
