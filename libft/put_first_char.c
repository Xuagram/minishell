/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_first_char.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 11:28:50 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_neg(char *str)
{
	if (str[0] == '-')
		return (1);
	else
		return (0);
}

char	*apply_upcase(char *var, char conv)
{
	int i;

	i = 0;
	if (conv == 'X')
	{
		while (var[i])
		{
			var[i] = ft_toupper(var[i]);
			i++;
		}
	}
	return (var);
}

char	*put_sign(int neg, char *new)
{
	if (!neg)
		*new = '+';
	else
		*new = '-';
	return (new);
}

char	*put_sharp(t_arg arg, char *new)
{
	char	*tmp;

	tmp = new;
	if (arg.conv == 'x' || arg.conv == 'X' || arg.conv == 'o'
			|| arg.conv == 'p')
		*tmp = '0';
	if (arg.conv == 'X' || arg.conv == 'x' || arg.conv == 'p')
	{
		tmp++;
		*tmp = (arg.conv == 'X') ? 'X' : 'x';
	}
	return (new);
}

char	*put_first_char(t_arg arg, t_first_c first_c, char *new)
{
	if (first_c.neg || arg.plus)
		new = put_sign(first_c.neg, new);
	if (arg.sharp)
		new = put_sharp(arg, new);
	if (arg.space && !(first_c.neg))
		*new = ' ';
	return (new);
}
