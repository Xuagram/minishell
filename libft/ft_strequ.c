/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 12:48:11 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/22 15:07:24 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strequ(char const *s1, char const *s2)
{
	if (s1 && s2)
	{
		while (*s1 == *s2 && *s1 && *s2)
		{
			s1++;
			s2++;
		}
		if (*s1 == '\0' && *s2 == '\0')
			return (1);
		else
			return (0);
	}
	return (0);
}

/*
**  static int	main(int argc, char **argv)
**  {
**  ft_strequ(argv[1], argv[2]);
**  return (0);
**  }
*/
