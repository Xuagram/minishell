/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_size_long.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 12:27:38 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		manage_oct(char *var, t_arg *arg)
{
	return ((var[0] == '0' && arg->conv == 'o') ? 0 : arg->sharp);
}

void	fill_size_long(t_arg *arg)
{
	ft_bzero(arg->len, 3);
	ft_memcpy(arg->len, "L", 1);
}

char	*c_conv(char *s, va_list ap, t_arg *arg)
{
	char c;

	if (!(s = ft_strnew(2)))
		exit(0);
	c = va_arg(ap, unsigned int);
	if (c == 0)
		arg->null = 1;
	else
		s[0] = c;
	return (s);
}

void	manage_print(char *var, t_arg arg, int *ret)
{
	char	c;

	c = 0;
	ft_putstr(var);
	if (arg.null == 1)
	{
		ft_putchar(c);
		*ret += 1;
	}
}

char	*integer_prec(char *new, long int prec, int neg, char *var)
{
	unsigned long int	i;

	i = -1;
	if (!(new = ft_strnew(prec + ft_strlen(var) + 1)))
		exit(0);
	while (++i < (prec - ft_strlen(var)))
		new[i] = '0';
	if (neg)
		new[i] = '0';
	ft_strcpy(new + i + neg, var + neg);
	ft_strdel(&var);
	return (new);
}
