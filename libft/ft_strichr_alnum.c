/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strichr_alnum.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/25 15:38:58 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:02:13 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strichr_alnum(const char *s)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = (char *)s;
	while (ft_isalnum(tmp[i]) && tmp[i] != '\0')
		i++;
	if (ft_isalnum(tmp[i]))
		return (0);
	return (i);
}
