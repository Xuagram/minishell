/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 17:17:32 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		letter;
	unsigned const char *tmp;

	letter = c;
	tmp = s;
	while (n--)
	{
		if (*tmp == letter)
			return ((void *)tmp);
		tmp++;
	}
	return (NULL);
}
/*
**#include <string.h>
**#include <stdio.h>
**
**int	main(int argc, char **argv)
**{
**	ft_printf("%s\n", ft_memchr(argv[1], ft_atoi(argv[2]), ft_atoi(argv[3])));
**	ft_printf("%s\n", memchr(argv[1], ft_atoi(argv[2]), ft_atoi(argv[3])));
**	return (0);
**}
*/
