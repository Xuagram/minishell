/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 11:40:44 by temehenn          #+#    #+#             */
/*   Updated: 2019/07/31 15:59:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	fill_prec_field(char **format, t_arg *arg)
{
	if (ft_isdigit(**format) && *(*format - 1) != '.')
	{
		arg->field = ft_atoi((*format));
		while (ft_isdigit(**format) && **format)
			(*format)++;
		(*format)--;
	}
	else if (**format == '.')
	{
		(*format)++;
		arg->prec = ft_atoi(*format);
		while (ft_isdigit(**format) && **format)
			(*format)++;
		if (arg->prec == 0)
			arg->prec = 0;
		(*format)--;
	}
}

void	fill_size(char **format, t_arg *arg)
{
	if (**format == 'h' && *(*format + 1) != 'h')
	{
		ft_bzero(arg->len, 3);
		ft_memcpy(arg->len, "h", 1);
	}
	else if (**format == 'h' && *(*format + 1) == 'h')
	{
		ft_memcpy(arg->len, "hh", 2);
		(*format)++;
	}
	else if (**format == 'l' && *(*format + 1) != 'l')
	{
		ft_bzero(arg->len, 3);
		ft_memcpy(arg->len, "l", 1);
	}
	else if (**format == 'l' && *(*format + 1) == 'l')
	{
		ft_memcpy(arg->len, "ll", 2);
		(*format)++;
	}
	else if (**format == 'L')
		fill_size_long(arg);
	else
		fill_prec_field(format, arg);
}

void	parse_arg(char **format, t_arg *arg)
{
	reset_arg(arg);
	(*format)++;
	while (**format && (is_flag(**format) || ft_isdigit(**format)
			|| **format == '.' || is_len_flag(**format, *(*format + 1))))
	{
		if (**format == '#')
			arg->sharp = 1;
		else if (**format == ' ')
			arg->space = 1;
		else if (**format == '0')
			arg->zero = 1;
		else if (**format == '+')
			arg->plus = 1;
		else if (**format == '-')
			arg->less = 1;
		else
			fill_size(format, arg);
		if (**format)
			(*format)++;
	}
	if (**format)
		arg->conv = **format;
}

void	manage_conflict(t_arg *arg)
{
	if (arg->less || ((is_unsigned_conv(arg->conv) || arg->conv == 'd'
				|| arg->conv == 'i') && arg->prec != -1))
		arg->zero = 0;
	if (arg->conv == 's' || arg->conv == 'c' || arg->conv == 'p')
		ft_bzero(arg->len, 3);
	if (arg->conv == 'd' || arg->conv == 'i' || arg->conv == 'u'
			|| arg->conv == 's' || arg->conv == 'c')
		arg->sharp = 0;
	if (arg->conv == 'p')
		arg->sharp = 1;
	if (!is_signed_conv(arg->conv))
	{
		arg->space = 0;
		arg->plus = 0;
	}
	if (arg->plus)
		arg->space = 0;
}

void	manage_arg(char **format, va_list ap, int *ret)
{
	char		*var;
	t_arg		arg;
	t_first_c	first_c;

	var = 0;
	first_c.neg = 0;
	parse_arg(format, &arg);
	manage_conflict(&arg);
	if (arg.conv)
	{
		if (is_convert(arg.conv))
			var = get_arg(ap, &arg);
		else
			var = ft_strncpy(ft_strnew(2), &(arg.conv), 1);
		first_c.neg = is_neg(var);
		var = apply_prec(var, arg.conv, arg.prec, first_c.neg);
		arg.sharp = manage_oct(var, &arg);
		var = apply_upcase(var, arg.conv);
		calc_len(var, arg, &first_c);
		var = put_string(var, arg, first_c);
		*ret += ft_strlen(var);
	}
	manage_print(var, arg, ret);
	ft_strdel(&var);
}
