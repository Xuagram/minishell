/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 20:14:53 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/16 16:28:12 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strnew(size_t size)
{
	char *new;

	if (!(new = (char *)ft_memalloc(size + 1)))
		return (NULL);
	ft_bzero(new, size);
	return (new);
}
