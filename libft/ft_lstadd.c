/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 22:42:33 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd(t_list **alst, t_list *new)
{
	if (!alst || !new)
		return ;
	else
	{
		new->next = *alst;
		*alst = new;
	}
}

/*
**#include "libft.h"
**static int	main(int argc, char **argv)
**{
**	t_list *lst;
**
**	lst = ft_lstnew(argv[1], sizeof(argv[1]));
**	ft_lstadd(&lst, ft_lstnew(argv[2], sizeof(argv[2])));
**	while (lst)
**	{
**		ft_printf("%s \n", lst->content);
**		lst = lst->next;
**	}
**	return (0);
**}
*/
