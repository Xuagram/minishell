/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/22 15:46:44 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	fill_number(char *res, long long int var, int base, int len)
{
	if (var >= base)
		fill_number(res, var / base, base, len - 1);
	if (var % base < 10)
		res[len] = var % base + 48;
	else
		res[len] = (var % base) % 10 + 'a';
}

int		get_len(long long int var, int base)
{
	int count;

	count = 0;
	while (var)
	{
		count++;
		var = var / base;
	}
	return (!count) ? 1 : count;
}

char	*itoa_base(long long int var, int base)
{
	char			*res;
	long long int	len;

	if (var == LONG_MIN)
	{
		if (!(res = ft_strdup("-9223372036854775808")))
			exit(0);
		return (res);
	}
	len = get_len(var, base);
	if (!(res = ft_strnew(len + 2)))
		exit(0);
	if (var < 0)
	{
		res[0] = '-';
		var = -var;
		len++;
	}
	fill_number(res, var, base, len - 1);
	return (res);
}
