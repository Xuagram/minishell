/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 22:59:15 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 11:12:29 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (s1[i])
		i++;
	while (s2[j] && (int)n > 0)
	{
		s1[i + j] = s2[j];
		j++;
		n--;
	}
	s1[i + j] = '\0';
	return (s1);
}
/*
** #include "libft.h"
** #include <string.h>
**int	main(int argc, char **argv)
**{
**	char s1[20] = "ciao bello";
**	char s2[20] = "grazie";
**	ft_putstr(ft_strncat(s1, s2, ft_atoi(argv[1])));
**}
*/
