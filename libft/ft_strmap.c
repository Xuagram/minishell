/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 11:21:10 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/22 15:11:50 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmap(char const *s, char (*f)(char))
{
	char *str;
	char *tmp_str;

	str = NULL;
	tmp_str = NULL;
	if (s)
	{
		if (!(str = ft_strnew(ft_strlen(s))))
			return (NULL);
		tmp_str = str;
		while (*s)
		{
			*str = f(*s);
			s++;
			str++;
		}
	}
	return (tmp_str);
}

/*
**static char	ft_next(char c)
**{
**	c = c + 1;
**	return (c);
**}
**
**static int	main(int argc, char **argv)
**{
**	ft_putstr(ft_strmap(argv[1], &ft_next));
**	return (0);
**}
*/
