/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   float.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 14:32:53 by temehenn          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <float.h>
#include <limits.h>

int			arrond(long double var)
{
	if (var == 5)
		return (arrond(var * 10 - 50));
	else if (var > 5)
		return (1);
	else if (var < 5)
		return (0);
	if (var > LDBL_MAX)
		return (0);
	else
		return (0);
}

static void	apply_arrond(int tab[1000], int end, int ret)
{
	if (!end)
	{
		ft_memmove(tab + 1, tab, 998 * sizeof(int));
		tab[end] = 1;
	}
	else if (tab[end] < 9)
	{
		tab[end] += ret;
		ret = 0;
	}
	else if (tab[end] == 9 && ret)
	{
		tab[end] = 0;
		apply_arrond(tab, end - 1, ret);
	}
}

int			extract_pe(long double var, int res[1000])
{
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (var > (long double)INT_MAX)
	{
		var /= 10;
		i++;
	}
	if (!i)
	{
		res[i] = (int)var;
		return (0);
	}
	while (i >= 0)
	{
		res[j] = (int)var;
		var -= res[j++];
		var *= 10;
		i--;
	}
	if (res[1] && arrond(var * 10) == 1)
		apply_arrond(res, j - 1, 1);
	return (j);
}

char		*fill_pe(char *res, int pe[9000], int j, t_arg *arg)
{
	char	*tmp;
	int		i;
	int		k;

	k = 1;
	tmp = itoa_base(pe[0], 10);
	i = ft_strlen(tmp);
	res = ft_strcpy(res, tmp);
	while (--j >= 0)
	{
		res[i] = pe[k] + '0';
		i++;
		k++;
	}
	if (arg->prec || arg->sharp)
		res[i] = '.';
	else
		res[i] = 0;
	res[i + 1] = 0;
	ft_strdel(&tmp);
	return (res);
}

char		*float_conv(long double var, t_arg *arg)
{
	char		*res;
	int			entire[9000];
	int			pe;

	if (arg->prec == -1)
		arg->prec = 6;
	ft_bzero(entire, 9000 * sizeof(int));
	pe = extract_pe(var, entire);
	if (!(res = (char *)malloc(sizeof(char) * 1000 + arg->prec + 2)))
		exit(0);
	ft_bzero(res, 1000 + arg->prec + 2);
	res = fill_pe(res, entire, pe - 1, arg);
	res = zero_case(var, res, arg->prec, arg->sharp);
	res = fill_pd(res, var, arg->prec, arg->sharp);
	return (res);
}
