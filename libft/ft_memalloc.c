/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 20:19:04 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/29 19:22:57 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void *supo;

	supo = 0;
	if (!(supo = malloc(sizeof(char) * size)) || size >= 2147483647)
		return (NULL);
	ft_bzero(supo, size);
	return (supo);
}
