/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_conv.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 14:30:57 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	is_random(char c, char d)
{
	return (!is_len_flag(c, d) && !is_flag(c) && c != '.' && !ft_isdigit(c)
			&& !is_signed_conv(c) && !is_unsigned_conv(c) && c != 'p'
			&& c != 's' && c != 'c') ? 1 : 0;
}

int	is_convert(char c)
{
	return (is_signed_conv(c) || is_unsigned_conv(c) || c == 'c'
			|| c == 's' || c == 'p') ? 1 : 0;
}
