/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strichr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/25 15:38:58 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:03:27 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"
#include <stdio.h>

int	ft_strichr(const char *s, char c)
{
	int		i;
	char	letter;
	char	*tmp;

	i = 0;
	letter = c;
	tmp = (char *)s;
	if (!tmp)
		return (-1);
	while (tmp[i] && tmp[i] != letter)
		i++;
	if (!tmp[i])
		return (-1);
	return (i);
}
