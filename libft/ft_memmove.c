/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 15:38:15 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 17:58:52 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char		*tmp_dest;
	const char	*tmp_src;

	tmp_dest = dest;
	tmp_src = src;
	if (!src && !dest)
		return (dest);
	if (tmp_dest < tmp_src)
	{
		while (n--)
			*tmp_dest++ = *tmp_src++;
	}
	else
		while (n--)
			*(tmp_dest + n) = *(tmp_src + n);
	return (dest);
}
/*
**#include <string.h>
**
**int	main(void)
**{
**	char str[20];
**	char str2[20] = "hello";
**
**	ft_putstr(memcpy(str+3, str+1, 3));
**	ft_putstr(ft_memcpy(str+3, str+1, 3));
**	ft_putstr(memmove(NULL, NULL, 3));
**	ft_putchar('\n');
**	ft_putstr(ft_memmove(NULL, str2, 3));
**	return(0);
**}
*/
