/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   div_case.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 13:29:24 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <math.h>

int		div_case(long double a, char **s)
{
	long double inf;
	long double inf2;

	inf = 1.0 / 0.0;
	inf2 = -1.0 / 0.0;
	if (!(a <= INFINITY))
	{
		if (!(*s = ft_strdup("nan")))
			exit(0);
	}
	else if (a == inf)
	{
		if (!(*s = ft_strdup("inf")))
			exit(0);
	}
	else if (a == inf2)
	{
		if (!(*s = ft_strdup("-inf")))
			exit(0);
	}
	return (*s) ? 1 : 0;
}

char	*zero_case(long double var, char *res, long int prec, int sharp)
{
	if (1.0 / var == -INFINITY)
	{
		ft_strdel(&res);
		if (sharp == 1 || prec != 0)
		{
			if (!(res = ft_strnew(100)))
				exit(0);
			res = ft_strcpy(res, "-0.");
		}
		else
		{
			if (!(res = ft_strnew(100)))
				exit(0);
			res = ft_strcpy(res, "-0");
		}
	}
	return (res);
}
