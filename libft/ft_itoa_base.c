/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 11:14:33 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 19:01:55 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static unsigned int		ft_check_b(char *base)
{
	unsigned int i;
	unsigned int j;

	i = 0;
	if (base[i])
	{
		while (base[i] != '\0')
		{
			j = i + 1;
			while (base[j] != '\0')
			{
				if ((base[j] == base[i]) || (base[i] == '+')
						|| (base[i] == '-') || (base[i] == ' ')
						|| (base[i] >= 9 && base[i] <= 13))
					return (0);
				j++;
			}
			i++;
		}
		if (i == 1)
			return (0);
		return (i);
	}
	return (0);
}

static size_t			ft_count_size(int n, int *neg, char *base)
{
	long int	nb;
	size_t		size;

	size = 1;
	nb = n;
	if (nb < 0)
	{
		nb *= -1;
		size++;
		*neg = 1;
	}
	while (nb >= ft_check_b(base))
	{
		size++;
		nb = nb / ft_check_b(base);
	}
	return (size);
}

char					*ft_itoa_base(int n, char *base)
{
	char		*new;
	size_t		size;
	long int	nb;
	int			neg;

	neg = 0;
	new = NULL;
	size = ft_count_size(n, &neg, base);
	nb = n;
	if (ft_check_b(base))
	{
		if (nb < 0)
			nb = -nb;
		if (!(new = (ft_strnew(size))))
			return (NULL);
		while (size > 0)
		{
			new[--size] = base[nb % ft_check_b(base)];
			nb = nb / ft_check_b(base);
		}
		if (neg)
			new[size] = '-';
	}
	return (new);
}

/*
**int	main(int argc, char **argv)
**{
**ft_putstr(ft_itoa_base(ft_atoi(argv[1]), argv[2]));
**return (0);
**}
*/
