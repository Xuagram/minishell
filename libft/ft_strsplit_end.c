/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_end.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 18:39:20 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/23 16:56:36 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	ft_line(char const *s, char c)
{
	int line;

	line = 0;
	while (*s)
	{
		if (*s != c && *s)
		{
			line++;
			while (*s != c && *s)
				s++;
		}
		else if (*s)
			s++;
	}
	return (line);
}

static char	**ft_dupstrsplit(char const *s, char **new, int line, char c)
{
	char	*s2;
	int		i;

	i = 0;
	while (i < line && s)
	{
		while (*s && *s == c)
			s++;
		s2 = ft_strchr_end(s, c);
		if (s2)
		{
			if (!(new[i++] = ft_strsub(s, 0, s2 - s)))
				return ((char **)ft_freetab((void **)new));
		}
		else
		{
			if (!(new[i++] = ft_strdup(s)))
				return ((char **)ft_freetab((void **)new));
		}
		s = s2;
	}
	new[i] = NULL;
	return (new);
}

char		**ft_strsplit_end(char const *s, char c)
{
	char	**new;
	int		line;

	if (!s)
		return (NULL);
	line = ft_line(s, c);
	if (!(new = (char **)malloc(sizeof(char *) * (line + 1))))
		return (NULL);
	new = ft_dupstrsplit(s, new, line, c);
	return (new);
}

/*
**int	main(int argc, char **argv)
**{
**	char **yolo;
**	int i = 0;
**	char str[] = "Los*Pollos*Hermanos";
**	yolo = ft_strsplit(str, '*');
**	while (yolo[i])
**	{
**		ft_printf("%s\n", yolo[i]);
**		i++;
**	}
**	i = 0;
**	ft_freetab((void **)yolo);
**	while (i < 4)
**	{
**		ft_printf("%s\n", yolo[i]);
**		ft_printf("%p\n", yolo[i]);
**		ft_printf("%d\n", i);
**		i++;
**	}
**	return (0);
**}
*/
