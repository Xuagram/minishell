/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 21:01:13 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strchr(const char *s, int c)
{
	unsigned char		letter;
	char				*tmp;

	letter = c;
	tmp = (char *)s;
	while (*tmp != letter && *tmp != '\0')
		tmp++;
	if (*tmp != letter)
		return (NULL);
	return ((char *)tmp);
}
/*
**int	main(int argc, char **argv)
**{
**	ft_printf("%s", ft_strchr(argv[1], ft_atoi(argv[2])));
**	ft_printf("%s", strchr(argv[1], ft_atoi(argv[2])));
**}
*/
