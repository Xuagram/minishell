/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 11:00:56 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

/*
**void	ft_del_list(void* content, size_t size)
**{
**	free(content);
**	ft_putendl("test");
**	(void)size;
**}
*/

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
	if (*alst)
	{
		del((*alst)->content, (*alst)->content_size);
		free((*alst));
		(*alst) = NULL;
	}
}

/*
**#include <stdio.h>
**#include "libft.h"
**int	main(int argc, char **argv)
**{
**	t_list *yolo;
**	yolo = ft_lstnew(argv[1], sizeof(argv[1]));
**	ft_lstdelone(&yolo, ft_del_list);
**	ft_printf("%p", yolo);
**	return (0);
**}
*/
