/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freetab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 10:15:57 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/17 15:19:03 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	**ft_freetab(void **tab)
{
	int i;

	i = 0;
	if (!tab)
		return (NULL);
	while (tab[i])
	{
		ft_memdel((void **)&(tab[i]));
		i++;
	}
	free(tab);
	tab = NULL;
	return (tab);
}
