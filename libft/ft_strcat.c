/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 22:59:15 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/16 16:35:03 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *s1, const char *s2)
{
	char *tmp_s1;

	tmp_s1 = s1;
	while (*s1)
		s1++;
	while (*s2)
	{
		*s1 = *s2;
		s1++;
		s2++;
	}
	*s1 = *s2;
	return (tmp_s1);
}
/*
**int	main(int argc, char **argv)
**{
**	char s1[20] = "";
**	char s2[20] = "";
**	puts(strcat(s1, s2));
**}
*/
