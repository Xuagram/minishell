/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_free.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 13:32:43 by mahaffne          #+#    #+#             */
/*   Updated: 2019/09/24 14:04:17 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_free(char **s1, char *s2)
{
	char *tmp;

	tmp = NULL;
	if (!(tmp = ft_strjoin(*s1, s2)))
		return (NULL);
	ft_strdel(&*s1);
	return (tmp);
}
