/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 10:21:54 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strstr(const char *haystack, const char *needle)
{
	const char *hs_buff;
	const char *needle_buff;

	hs_buff = haystack;
	needle_buff = needle;
	if (!(*needle))
		return ((char *)haystack);
	while (*haystack)
	{
		hs_buff = haystack;
		while (*haystack == *needle && *needle)
		{
			haystack++;
			needle++;
		}
		if (!(*needle))
			return ((char *)hs_buff);
		needle = needle_buff;
		haystack = hs_buff;
		haystack++;
	}
	return (NULL);
}
/*
**#include <stdio.h>
**#include <string.h>
**
**int main(void)
**{
**
**	ft_printf("%s", ft_strstr(NULL, ""));
**	return (0);
**}
*/
