/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 09:53:05 by temehenn          #+#    #+#             */
/*   Updated: 2019/08/20 11:22:05 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <limits.h>
# include <float.h>

typedef struct			s_arg
{
	char				conv;
	char				len[3];
	short				sharp;
	short				space;
	short				zero;
	short				plus;
	short				less;
	long int			prec;
	long int			field;
	int					null;
}						t_arg;

typedef struct			s_first_c
{
	int					neg;
	unsigned long int	len;
	long int			space;
}						t_first_c;

char					*integer_prec(char *new, long int prec,
										int neg, char *var);
void					manage_print(char *var, t_arg arg, int *ret);
char					*c_conv(char *s, va_list ap, t_arg *arg);
char					*float_conv(long double a, t_arg *arg);
int						arrond(long double var);
char					*zero_case(long double var, char *res,
									long int prec, int sharp);
int						div_case(long double a, char **s);
char					*fill_pd(char *res, long double var,
									long int prec, int sharp);
char					*apply_prec(char *var, char conv,
									long int prec, int neg);
char					*apply_upcase(char *var, char conv);
char					*put_string(char *var, t_arg arg,
									t_first_c first_c);
char					*put_first_char(t_arg arg,
									t_first_c first_c, char *new);
int						calc_len(char *var, t_arg arg,
									t_first_c *first_c);
char					*itoa_base(long long int nb, int base);
char					*utoa_base(unsigned long long int nb, int base);
int						is_signed_conv(char c);
int						is_unsigned_conv(char c);
int						is_len_flag(char c, char d);
int						is_flag(char c);
int						is_conv(char c, char d);
int						is_random(char c, char d);
int						is_convert(char c);
int						is_neg(char *str);
int						ft_printf(char *format, ...);
int						core(char *format, va_list ap);
int						check_arg(char *format);
int						is_conv_size(char c, char d);
int						manage_oct(char *var, t_arg *arg);
void					manage_arg(char **format, va_list ap, int *ret);
void					fill_size_long(t_arg *arg);
void					reset_arg(t_arg *arg);
void					print_arg(t_arg *arg);
char					*get_arg(va_list ap, t_arg *arg);

#endif
