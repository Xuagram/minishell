/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 16:53:27 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 18:57:13 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	if (!lst && !(f))
		return ;
	while (lst)
	{
		f(lst);
		lst = lst->next;
	}
}

/*
**int	main(int argc, char **argv)
**{
**	t_list *lst;
**
**	lst = ft_lstnew(argv[1], sizeof(argv[1]));
**	ft_lstadd(&lst, ft_lstnew(argv[2], sizeof(argv[2])));
**	ft_lstiter(lst, ft_lstprint);
**	return (0);
**}
*/
