/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 12:13:12 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/23 18:58:42 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

/*
**t_list	*ft_yoli(t_list *elem)
**{
**	t_list *new;
**
**	new = ft_lstnew(elem, sizeof(elem));
**	new->content = "salut";
**	return (new);
**}
*/

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *begin_map;

	begin_map = NULL;
	if (!f || !lst)
		return (begin_map);
	while (lst)
	{
		if (!begin_map)
			begin_map = f(lst);
		else
			ft_lst_push_back(&begin_map, f(lst));
		lst = lst->next;
	}
	return (begin_map);
}

/*
**int	main(int argc, char **argv)
**{
**	t_list *lst = ft_lstnew(argv[1], sizeof(argv[1]));
**	ft_lstadd(&lst, ft_lstnew(argv[2], sizeof(argv[2])));
**	ft_lstiter(ft_lstmap(lst, ft_yoli), ft_lstprint);
**	return (0);
**}
*/
