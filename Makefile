# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/21 13:40:41 by mahaffne          #+#    #+#              #
#    Updated: 2019/09/25 14:14:27 by mahaffne         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=minishell
CC=gcc -g
LIBFTDIR= ./libft
SRCDIR = src
OBJDIR = obj
FLAGS= -Wall -Wextra -Werror -g
SRCFILE = 	binary.c\
			built_in.c\
			check_dollar.c\
			chg_dir.c\
			concat_path.c\
			error.c\
			echo.c\
			expansions.c\
			env.c\
			exit.c\
			get_opt.c\
			init.c\
			main.c\
			minishell.c\
			parsing.c\
			path.c\
			set_env.c\
			set_pwd.c\
			quote.c\
			unsetenv.c\
			valid_var.c\

SRC = $(addprefix $(SRCDIR), $(SRCFILE))
OBJFILE = $(SRCFILE:.c=.o)
OBJ = $(addprefix $(OBJDIR)/,$(OBJFILE))

all: $(NAME)

$(NAME): $(OBJ)
	@$(CC)  -o $(NAME) $(OBJ) ./libft/libft.a
	@echo "\033[32mCompilation SUCCEED. Binary created :\033[32;1m" $@ "\033[00m"

$(OBJ): | $(OBJDIR)

$(OBJDIR):
	@make -C libft/
	@mkdir $@

$(OBJDIR)/%.o :$(SRCDIR)/%.c
	@$(CC) $(FLAGS) -o $@ -c $< -I include/ -I $(LIBFTDIR)
	@echo "File compiled : " $<

clean:
	@make -C libft/ clean
	@rm -rf $(OBJDIR)
	@echo "\033[36mObjects have been deleted.\033[00m"

fclean: clean
	@make -C libft/ fclean
	@rm -f $(NAME)
	@echo "\033[36m"$(NAME) "have been deleted.\033[00m"

re: fclean all

.PHONY: all clean fclean re
